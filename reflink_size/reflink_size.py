#!/usr/bin/env python3
# This Software is Licensed under the MIT License
# Copyright (c) 2023 Cyber Secure Pty Ltd

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import subprocess
from reflink import SharedExtentAllocation
import os
import time
from glob import glob
import logging
logger = logging.getLogger()


console_handler = logging.StreamHandler() # sys.stderr
console_handler.setFormatter( logging.Formatter('[%(levelname)s](%(name)s): %(message)s') )
logger.addHandler(console_handler)

shared_blocks = dict()
files = dict()
blocksize = 4096
extents = list()
blocks_allocated = dict()
extent_current = 0

import math




def convert_size(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])

def set_log_level_from_verbose(args):
    print(args)
    if not args.verbose:
        logger.setLevel('ERROR')
    elif args.verbose == 1:
        logger.setLevel('WARNING')
    elif args.verbose == 2:
        logger.setLevel('INFO')
    elif args.verbose >= 3:
        logger.setLevel('DEBUG')
    else:
        logger.critical("UNEXPLAINED NEGATIVE COUNT!")

        
def printfile_percent(file):    
    if file['fromcache']:
        cached = "(Cached)"
    else:
        cached = ""
    print("{} : Total: {}, Unique: {:.1f}%, Shared (new/others): {:.1f}%/{:.1f}% Time: {:.2f} {}".format
            (file['filename'],
            convert_size(file['totalblocks']*blocksize),
            file['uniqueblocks']/file['totalblocks']*100 ,
            file['sharedblocks_original']/file['totalblocks']*100,
            file['sharedblocks']/file['totalblocks']*100,
            file['time_taken'],
            cached,
            ))    
def get_files_recursive(dir):    # dir: str, ext: list
    files = []
    subfolders = []
    for f in os.scandir(dir):
        if f.is_dir():
            subfolders.append(f.path)
        if f.is_file():
            files.append(f.path)


    for dir in list(subfolders):
        f = get_files_recursive(dir)
        files.extend(f)
    return files


def main():
    total_unique_blocks = 0
    sharedextents = SharedExtentAllocation()
    files_list = list()
    for filename in args.files:
        if args.recursive:
            files_list.extend(get_files_recursive(filename))
        else:
            files_list.extend((glob(filename)))
    command_line_args =  ["filefrag","-v"] + files_list
    logger.info("Running filefrag")
    logger.debug("Running " + str(command_line_args))
    output = subprocess.run(command_line_args,capture_output=True,encoding='utf-8')
    filename = None
    filestart = time.time()
    filecache = dict()
    # Load the file cache.
    if args.scope is not None:
        cachefilename = args.cachedir + ".reflink.cache-"+args.scope
        try:
            filecache = sharedextents.load(cachefilename)
            sharedextents.recalculate_totals()
            logger.info("Loaded Cache for scope "+args.scope)
        except:
            logger.warning("No Cache/failed Cache for scope "+args.scope)
            filecache = dict()
    skipfile = False
    cachetime = time.time()
    for line in output.stdout.splitlines():
        if 'Filesystem' in line:
            continue
        if filename is not None and filename in line:
            if filename is not None:
                if not skipfile:
                    (total_shared_blocks,original_shared_blocks,shared_shared_blocks) = sharedextents.add(files[filename]['shared_extents'])
                    files[filename]['sharedblocks_original'] = original_shared_blocks
                    files[filename]['sharedblocks'] = files[filename]['totalblocks'] - original_shared_blocks - files[filename]['uniqueblocks']
                total_unique_blocks += files[filename]['uniqueblocks']
                files[filename]['time_taken'] = time.time() - filestart
                if not args.nofiledetail:printfile_percent(files[filename])
                if (time.time() - cachetime) > 30 and args.scope is not None:
                    logger.info("Saving progress in Cache.")
                    sharedextents.save(cachefilename,files)
                    cachetime = time.time()
                filestart = time.time()
                continue
                        

        if 'File' in line:
            if skipfile:
                skipfile = False
            filedata = line.split(" ")
            filename = str(filedata[3])
            files[filename] = dict()
            files[filename]['fromcache'] = False
            files[filename]['filename'] = filename
            if filename in filecache:
                stat = os.stat(filename)

                if ('modifytime' in filecache[filename].keys() and
                    filecache[filename]['modifytime'] == stat.st_mtime and
                    filecache[filename]['size']  == stat.st_size):
                    files[filename] = filecache[filename]
                    files[filename]['fromcache'] = True
                    filecache[filename]['used'] = True
                    skipfile = True
                else: 
                    #print(filecache[filename]['modifytime'],stat.st_mtime)
                    #print(filecache[filename]['size'],stat.st_size)
                    logger.warning("File cache for {} is stale, removing.".format(filename))
                    if filecache[filename]['sharedblocks'] != 0:
                        if len(filecache[filename]['shared_extents']) == 0:
                            logger.error("Error with File cache - expected ",filecache[filename]['sharedblocks']," shared blocks but no extents")
                            logger.error(print(filecache[filename]))
                        else:
                            sharedextents.remove(filecache[filename]['shared_extents'])
                    del filecache[filename]

            if filename not in filecache:
                files[filename]['totalsize'] = filedata[5]
                files[filename]['shared_extents'] = list()
                files[filename]['totalblocks'] = int(filedata[6].replace('(',''))
                files[filename]['sharedblocks'] = 0
                files[filename]['sharedblocks_original'] = 0
                files[filename]['uniqueblocks'] = 0
                stat = os.stat(filename)
                files[filename]['modifytime'] = stat.st_mtime
                files[filename]['size'] = stat.st_size
            continue
        if 'physical_offset' in line or 'extents' in line: # Header line
            continue
        if skipfile:
            continue
        linedata = line.split(':')
        blockrange = linedata[2].split('..')
        start = int(blockrange[0].strip())
        end = int(blockrange[1])
        if 'shared' in line:
            files[filename]['shared_extents'].append((start,end))
        else:
            files[filename]['uniqueblocks'] += end-start
    if len(filecache) > 0:
        cleaned = False
        for filename in filecache.keys():
            file = filecache[filename]
            first = False
            if 'used' not in file.keys() or not file['used']:
                if first: 
                    logger.warning("Removing Unused file data from scope:" + args.scope)
                    first = False
                    cleaned = True
                if len(file['shared_extents']) > 0:
                    logger.warning("Removing " + filename + ": " + str(len(file['shared_extents'])) + " extents")
                    sharedextents.remove(file['shared_extents'])
    if args.scope is not None:
        sharedextents.save(cachefilename,files)
    stats = dict()
    stats['diskused'] = int((sharedextents.ondisk_blocks+total_unique_blocks)*blocksize)
    stats['totalsize'] = int((sharedextents.allocated_blocks+total_unique_blocks)*blocksize)
    stats['avgfileused'] = stats['diskused']/len(files)
    stats['avgfilesize'] = stats['totalsize']/len(files)
    stats['files'] = len(files)
    shell_name_map = {
        'diskused': 'TOTAL_DISK_USED',
        'totalsize': 'TOTAL_SIZE',
        'avgfileused': 'AVG_DISK_USED_PER_FILE',
        'avgfilesize': 'AVG_SIZE_PER_FILE',
        'files': 'NUM_FILES',
    }
    if args.savedatafile is not None:
        with open(args.savedatafile,"w") as datafile:
            for stat in stats.keys():          
                datafile.write(shell_name_map[stat]+"="+str(stats[stat])+"\n")

                

    print("Files {} : Disk Used/size: {}/{}, Avg file: {}/{}  ".format(
                len(files),
                convert_size(stats['diskused']),
                convert_size(stats['totalsize']),
                convert_size(stats['avgfileused']) ,
                convert_size(stats['avgfilesize']),
                ))



import argparse
parser = argparse.ArgumentParser(description="Reflink shared block calculator. Uses filefrag to get actual disk usage.",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-s", "--scope",help="scope for cache - will use this as a reference for shared blocks",dest='scope')
parser.add_argument("--cache-dir", help="dir to save cache",default="/tmp/",dest='cachedir')
parser.add_argument("--save-data", help="file to save data (in shell vars form)",dest='savedatafile')
parser.add_argument('-v', '--verbose', action="count", help="verbose level... repeat up to three times.")
parser.add_argument('--nofiledetail', action="store_true", help="Don't show file detail")
parser.add_argument('-r', '--recursive', action="store_false", help="recursive file patterns")

parser.add_argument("files", help="File(s) to analyse for shared blocks",nargs='+')


args = parser.parse_args()
config = vars(args)
set_log_level_from_verbose(args)
main()

