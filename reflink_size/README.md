There are no native tools that allow you to calculate the usage of a subset of files that are using reflink. This is particularly useful to do if you're backing up multiple clients with Veaam.

You will need numpy and numba - This is a very computationally expensive process.


```
usage: reflink_size.py [-h] [-s SCOPE] [--cache-dir CACHEDIR] [--save-data SAVEDATAFILE] [-v] [--nofiledetail] [-r] files [files ...]

Reflink shared block calculator. Uses filefrag to get actual disk usage.

positional arguments:
  files                 File(s) to analyse for shared blocks

optional arguments:
  -h, --help            show this help message and exit
  -s SCOPE, --scope SCOPE
                        scope for cache - will use this as a reference for shared blocks (default: None)
  --cache-dir CACHEDIR  dir to save cache (default: /tmp/)
  --save-data SAVEDATAFILE
                        file to save data (in shell vars form) (default: None)
  -v, --verbose         verbose level... repeat up to three times. (default: None)
  --nofiledetail        Don't show file detail (default: False)
  -r, --recursive       recursive file patterns (default: True)
```

Basic usage: 
```
# ./reflink_size.py /example_dir/
/example_dir/MDD2023-09-04T141514_99FF.vbk : Total: 130.43 GB, Unique: 9.1%, Shared (new/others): 90.9%/0.0% Time: 0.36
/example_dir/MDD2023-09-06T000007_9944.vib : Total: 8.89 GB, Unique: 81.7%, Shared (new/others): 18.2%/0.1% Time: 0.02
/example_dir/MDD2023-09-07T000007_7FD2.vib : Total: 7.93 GB, Unique: 82.3%, Shared (new/others): 17.6%/0.1% Time: 0.02
/example_dir/MDD2023-09-08T014933_00BD.vbk : Total: 130.55 GB, Unique: 5.8%, Shared (new/others): 1.1%/93.2% Time: 1.81
/example_dir/MDD2023-09-10T011650_3D5C.vbk : Total: 130.54 GB, Unique: 5.8%, Shared (new/others): 12.2%/82.0% Time: 2.22
/example_dir/MDD2023-09-11T000020_714C.vib : Total: 7.85 GB, Unique: 100.0%, Shared (new/others): 0.0%/0.0% Time: 0.00
/example_dir/MD_139A6.vbm : Total: 364.0 KB, Unique: 98.9%, Shared (new/others): 0.0%/1.1% Time: 0.00
Files 9 : Disk Used/size: 196.42 GB/440.93 GB, Avg file: 21.82 GB/48.99 GB
```
Advanced usage:

If you give it a scope with -s, it will save a cache file, eg:
```
# ./reflink_size.py -s example /example_dir/
/example_dir/MDD2023-09-04T141514_99FF.vbk : Total: 130.43 GB, Unique: 9.1%, Shared (new/others): 90.9%/0.0% Time: 0.35
/example_dir/MDD2023-09-06T000007_9944.vib : Total: 8.89 GB, Unique: 81.7%, Shared (new/others): 18.2%/0.1% Time: 0.02
/example_dir/MDD2023-09-07T000007_7FD2.vib : Total: 7.93 GB, Unique: 82.3%, Shared (new/others): 17.6%/0.1% Time: 0.02
/example_dir/MDD2023-09-08T014933_00BD.vbk : Total: 130.55 GB, Unique: 5.8%, Shared (new/others): 1.1%/93.2% Time: 1.82
/example_dir/MDD2023-09-09T000004_E2DC.vib : Total: 8.91 GB, Unique: 71.5%, Shared (new/others): 28.4%/0.1% Time: 0.02
/example_dir/MDD2023-09-10T011650_3D5C.vbk : Total: 130.54 GB, Unique: 5.8%, Shared (new/others): 12.2%/82.0% Time: 2.21
/example_dir/MDD2023-09-11T000020_714C.vib : Total: 7.85 GB, Unique: 100.0%, Shared (new/others): 0.0%/0.0% Time: 0.00
/example_dir/MD_139A6.vbm : Total: 364.0 KB, Unique: 98.9%, Shared (new/others): 0.0%/1.1% Time: 0.00
Files 9 : Disk Used/size: 196.42 GB/440.93 GB, Avg file: 21.82 GB/48.99 GB
```
```
# ./reflink_size.py -s example --save-data example.sh /example_dir/
/example_dir/MDD2023-09-04T141514_99FF.vbk : Total: 130.43 GB, Unique: 9.1%, Shared (new/others): 90.9%/0.0% Time: 0.21 (Cached)
/example_dir/MDD2023-09-06T000007_9944.vib : Total: 8.89 GB, Unique: 81.7%, Shared (new/others): 18.2%/0.1% Time: 0.00 (Cached)
/example_dir/MDD2023-09-07T000007_7FD2.vib : Total: 7.93 GB, Unique: 82.3%, Shared (new/others): 17.6%/0.1% Time: 0.00 (Cached)
/example_dir/MDD2023-09-08T014933_00BD.vbk : Total: 130.55 GB, Unique: 5.8%, Shared (new/others): 1.1%/93.2% Time: 0.00 (Cached)
/example_dir/MDD2023-09-09T000004_E2DC.vib : Total: 8.91 GB, Unique: 71.5%, Shared (new/others): 28.4%/0.1% Time: 0.00 (Cached)
/example_dir/MDD2023-09-10T011650_3D5C.vbk : Total: 130.54 GB, Unique: 5.8%, Shared (new/others): 12.2%/82.0% Time: 0.00 (Cached)
/example_dir/MDD2023-09-11T000020_714C.vib : Total: 7.85 GB, Unique: 100.0%, Shared (new/others): 0.0%/0.0% Time: 0.00 (Cached)
/example_dir/MD_139A6.vbm : Total: 364.0 KB, Unique: 98.9%, Shared (new/others): 0.0%/1.1% Time: 0.00 (Cached)
Files 9 : Disk Used/size: 196.42 GB/440.93 GB, Avg file: 21.82 GB/48.99 GB

# ls -lh /tmp/.reflink.cache-*
-rw-r--r-- 1 root      root      1.7M Sep 12 09:53 /tmp/.reflink.cache-example


# cat example.sh
TOTAL_DISK_USED=210908770304
TOTAL_SIZE=473445851136
AVG_DISK_USED_PER_FILE=23434307811.555557
AVG_SIZE_PER_FILE=52605094570.666664
NUM_FILES=8
```


