#!/usr/bin/env python3
# This Software is Licensed under the MIT License
# Copyright (c) 2023 Cyber Secure Pty Ltd

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import subprocess
import os
import numpy as np
from numba import jit
from array import array 
import pickle
import shutil
import logging
logger = logging.getLogger('reflink')

shared_blocks = dict()
files = dict()
blocksize = 4096
extents = list()
blocks_allocated = dict()
extent_current = 0

import math


INTERSECT_NONE = int(0)
INTERSECT_CONTAINED = int(1)
INTERSECT_START_BEFORE_END_IN = int(2)
INTERSECT_START_IN_END_AFTER = int(3)
INTERSECT_START_AFTER_END_AFTER = int(4)
INTERSECT_NONE_AFTER = int(5)

class SharedExtentAllocation():
    def save(self,filename,files):
        pickleme = (self.extents,files)
        with open(filename+".tmp","wb") as file:
            pickle.dump(pickleme, file)
        # make it atomic so if we're killed we don't lose the cache
        shutil.move(filename+".tmp",filename)
    
    def load(self,filename):
        with open(filename,"rb") as file:
            (self.extents,files) = pickle.load(file)
        return files

    def __init__(self):
        self.extents = list()
        self.npextents = np.empty((0,3), int)
        self.extents_deferred = list()
        self.allocated_blocks = int()
        self.ondisk_blocks = int()
        self.extents_metrics = {
            'non_colliding': 0,
            'pre': 0,
            'post': 0,
            'gaps': 0,
            'collision_ends_in': 0,
            'collision_ends_after': 0,
            'collision_starts_before_ends_after': 0,
            'collision_contained':0,
            'max_collisions_per_extent': 0,


        }
    
    def add(self,extents):
        self.collisions = 0
        extents_cur_idx = 0 # track where we are in the existing extents.
        np_extents = np.array(extents,dtype=[('start','int64'),('end','int64')]) #,('refcount','int16')])
        np_extents.sort(order='start')
        start_ondisk_blocks = self.ondisk_blocks
        start_allocated_blocks = self.allocated_blocks
        count = 0
        if len(self.extents) != 0:
            self.est_period = (self.extents[-1][1] - self.extents[0][0])/len(self.extents)
        for newextent in np_extents:
            count += 1
            extents_cur_idx = self.allocate_extent(newextent,extents_cur_idx)


        new_shared_blocks = (self.ondisk_blocks-start_ondisk_blocks)
        new_allocated_blocks = (self.allocated_blocks-start_allocated_blocks)
        return (self.allocated_blocks,new_shared_blocks,new_allocated_blocks)

    def remove(self,extents):
        # We assume all the extents are already chopped enough, so the only thing we do is decrease refcount, and remove if zero.
        np_extents = np.array(extents,dtype=[('start','int64'),('end','int64')]) #,('refcount','int16')])
        np_extents.sort(order='start')
        self.est_period = (self.extents[-1][1] - self.extents[0][0])/len(self.extents)
        logger.info("Removing ",len(np_extents)," extents")
        rm_idx = self._get_starting_index(np_extents[0][0])
        for rmextent in np_extents:
            (rmstart,rmend) = rmextent

            while(True):
                (existing_start,existing_end, none) = self.extents[rm_idx]
                intersection = get_intersection_type(rmstart,rmend,existing_start,existing_end)
                if intersection == INTERSECT_NONE_AFTER:
                    break
                elif intersection == INTERSECT_NONE:
                    rm_idx += 1
                else:
                    self._adjust_extent(rm_idx,refcount=-1)
                    rm_idx += 1
        self.recalculate_totals()
                
            
        
    def _get_starting_index(self,startblock):
        if len(self.extents) < 10:
            return 0
        target_idx = int((startblock-self.extents[0][0])/self.est_period)
        target_skip = 0
        while target_idx > 0 and target_idx < len(self.extents) and self.extents[target_idx][0] > startblock:
            target_idx -= int(len(self.extents)/100) # skip back about 1%
            target_skip += 1
        return max(target_idx,0) # no negative indexes

    def allocate_extent(self,newextent,extent_idx = 0):
        colliding_extents = list()

        # Skip ahead an approximate amount so we don't waste time.
        if extent_idx == 0 and len(self.extents) != 0:
            extent_idx = self._get_starting_index(newextent[0])

        prev_start = 0
        search_offset=0
        # we go back three to make sure we get duplicate start points.

        search_offset=min(3,len(self.extents))
        if extent_idx < search_offset:
            search_offset=extent_idx
        logger.debug("searching collisions from ",extent_idx,"-",search_offset," to ",len(self.extents))
        for index in range(extent_idx-search_offset,len(self.extents)):
            start = self.extents[index][0]
            end = self.extents[index][1]
            if start < prev_start:
                logger.error(str(self.extents[index-2:index+3]))
                logger.fatal("Unsorted  entry in array (",start,"<",prev_start,") Something is wrong", index)
                exit()
            prev_start = start
            intersection = get_intersection_type(newextent[0],newextent[1],start,end)
            if (intersection == INTERSECT_NONE_AFTER): # if it starts after the end, we've gone too far, stop.
                break
            elif (intersection != INTERSECT_NONE):
                self.collisions += 1
                colliding_extents.append((index,intersection))
            extent_idx = index
        
        if len(colliding_extents) == 0:
            logger.debug("Adding Non Colliding extent",search_offset,extent_idx,newextent)
            self.extents_metrics['non_colliding'] += 1
            self._insert_extent_deferred([newextent[0],newextent[1],1],extent_idx)
            extent_idx += 1
            self._insert_deferred_extents()
        else:
            

            first_collision_start = self.extents[colliding_extents[0][0]][0]
            if first_collision_start > newextent[0]:
                logger.debug("Inserting pre collisions extent")
                self.extents_metrics['pre'] += 1
                self._insert_extent_deferred([newextent[0],first_collision_start-1,1],colliding_extents[0][0])
                
            prevend=None
            for colextent in colliding_extents:
                (colextentidx,collision_type) = colextent
                start = self.extents[colextentidx][0]
                end = self.extents[colextentidx][1]
                refcount = self.extents[colextentidx][2]
                if prevend is not None and start-prevend > 1:
                    logger.debug("Gap Filler Extent ",[prev[1]+1,start-1],colextentidx,prev)
                    self._insert_extent_deferred([prev[1]+1,start-1,1],colextentidx) # Fill in the gaps.
                    self.extents_metrics['gaps'] += 1
                prev = self.extents[colextentidx]
                if collision_type == INTERSECT_CONTAINED:
                    logger.debug("completely contained - increasing refcount of "+ str(colextentidx) + "{} {}".format(newextent,self.extents[colextentidx]))
                    # completely contained
                    self.extents_metrics['collision_contained'] += 1
                    self._adjust_extent(colextentidx,refcount=+1)
                # If start is before start and end is within new 
                elif collision_type == INTERSECT_START_BEFORE_END_IN:
                    self.extents_metrics['collision_ends_in'] += 1
                    logger.debug("Collision extent starts before start and ends within") 
                    # Set the extent to end before the start of the new extent
                    self._adjust_extent(colextentidx,end=newextent[0]-1)
                    # Insert a new extent with an increased refcount after the old one
                    self._insert_extent_deferred([newextent[0],end,refcount+1],colextentidx+1)
                # end is after end, and start is within new.
                elif collision_type == INTERSECT_START_IN_END_AFTER:
                    self.extents_metrics['collision_ends_after'] += 1
                    logger.debug("Collision extent ends after end,but starts within")    
                    # Set the extent to end at the end of the extent and increase refcount
                    self._adjust_extent(colextentidx,end=newextent[1],refcount=+1)
                    # Insert a new extent after with the same refcount
                    self._insert_extent_deferred([newextent[1]+1,end,refcount],colextentidx+1)
                elif collision_type == INTERSECT_START_AFTER_END_AFTER:
                    self.extents_metrics['collision_starts_before_ends_after'] += 1
                    logger.debug("Collision extent extends beyond start and end")    
                    # Set the original extent to end before the start
                    self._adjust_extent(colextentidx,end=newextent[0]-1)
                    # Insert extra extent after the new extent with the same refcount
                    self._insert_extent_deferred([newextent[1]+1,end,refcount+0],colextentidx+1)
                    # Insert new extent that ends at the end and increase refcount 
                    self._insert_extent_deferred([newextent[0],newextent[1],refcount+1],colextentidx+1)
 

            # if the last collision ends before the end, make an extent to cover that (and put it after it)
            last_collision_index = colliding_extents[-1][0]
            last_collision_end = self.extents[last_collision_index][1]
            if last_collision_end < newextent[1]:
                logger.debug("Inserting post collisions extent")
                self.extents_metrics['post'] += 1
                self._insert_extent_deferred([last_collision_end+1,newextent[1],1],last_collision_index+1)
            self._insert_deferred_extents()
        return extent_idx

    def _adjust_extent(self,extent_idx,start=None,end=None,refcount=None):
        '''refcount can be -1 or +1 to inc/dec'''
        # Adjust refcount first (because it's easier that way)
        logger.debug("adjusting extent {} from {}  to start {} end {} refcount change {}".format(extent_idx,self.extents[extent_idx],start,end,refcount))
        original_size= _get_extent_size(array('L',self.extents[extent_idx]))
    
        if refcount is not None:
            self.extents[extent_idx][2] += refcount
            if self.extents[extent_idx][2] < 0:
                self.extents[extent_idx][2] = 0
        if start is not None:
            self.extents[extent_idx][0] = start

        if end is not None:
            self.extents[extent_idx][1] = end
        
        newsize = _get_extent_size(array('L',self.extents[extent_idx]))
        

        self.ondisk_blocks += newsize[0]-original_size[0]
        self.allocated_blocks += newsize[1]-original_size[1]
        if self.allocated_blocks < 0:
            print("CHECKME:",self.allocated_blocks,original_size,newsize,extent_idx)
            raise Exception("Extent has gone negative, something is very wrong")
        # At 0 refcount, we might want to remove the extent, but we don't have to.
        if self.extents[extent_idx][2] == 0:
            logger.debug("extent zeroed " + str(extent_idx))
            pass
        
    def _insert_extent_deferred(self,newextent,extent_idx):
        logger.debug("Deferring Insert of {}-{} In position {} refcount {} ".format(
            newextent[0],
            newextent[1],
            extent_idx,
            newextent[2],
        ))

        self.extents_deferred.append([newextent,extent_idx])


        
    def _insert_deferred_extents(self):

        for extentdetail in self.extents_deferred:
            insert_idx = extentdetail[1]
            newextent = extentdetail[0]
            while insert_idx < len(self.extents) and self.extents[insert_idx][0] < newextent[0]:
                logger.debug("Skipping insert index",insert_idx,len(self.extents),newextent,self.extents[insert_idx])
                insert_idx += 1
            if insert_idx >= len(self.extents):
                insert_idx = -1
            self._insert_extent(newextent,insert_idx)
            #offset += 1
        self.extents_deferred = []

    def _insert_extent(self,newextent,extent_idx):
        logger.debug("Inserting {} In position {} refcount {} ".format(
             newextent[0],
             extent_idx,
             newextent[2],
           ))

        if len(newextent) != 3:
            raise Exception("Extent does not contain correct data"+str(newextent))
        if extent_idx==-1:
            self.extents.append(np.array(newextent))
        else:
            self.extents.insert(extent_idx,np.array(newextent))
        try:
            size = _get_extent_size(array('L',newextent))
        except:
            print(newextent)
            raise 
        self.ondisk_blocks += size[0]
        self.allocated_blocks += size[1]
        if self.allocated_blocks < 0:
            raise Exception("Allocated blocks are negative - did you wrap around?")
    
    def recalculate_totals(self):
        total_size = list((0,0)) #array('L',(0,0))
        count = 0
        for extent in self.extents:
            if len(extent) < 3:
                extent = [extent[0],extent[1],1]
            size = _get_extent_size(array('L',extent))
            total_size[0] += size[0]
            total_size[1] += size[1]
            if size[1] < 0:
                Exception("Size is negative, wrap error?",extent,size,count)
            count += 1
        self.ondisk_blocks = total_size[0]
        self.allocated_blocks = total_size[1]


@jit(nopython=True)
def _get_extent_size(extent: array('L')):
    ondisk_blocks = extent[1]-extent[0]+1
    allocated_blocks = ondisk_blocks*extent[2]
    return (ondisk_blocks,allocated_blocks)


INTERSECT_NONE = int(0)
INTERSECT_CONTAINED = int(1)
INTERSECT_START_BEFORE_END_IN = int(2)
INTERSECT_START_IN_END_AFTER = int(3)
INTERSECT_START_AFTER_END_AFTER = int(4)
INTERSECT_NONE_AFTER = int(5)


@jit(nopython=True)
def get_intersection_type(newstart: int,newend: int,start: int,end: int):
    
    if (start > newend):
        return int(5) #INTERSECT_NONE_AFTER
    elif newstart > end:
        return int(0) #INTERSECT_NONE
    elif start >= newstart and end <= newend:
        return int(1) #INTERSECT_CONTAINED
    # If start is before start and end is within new 
    elif start < newstart and end <= newend:
        return int(2) #INTERSECT_START_BEFORE_END_IN
    # end is after end, and start is within new.
    elif end > newend and start >= newstart:
        return int(3) #INTERSECT_START_IN_END_AFTER
    elif start < newstart and end > newend:
        return int(4) #INTERSECT_START_AFTER_END_AFTER

